package salariati.test;

import static org.junit.Assert.*;

import salariati.exception.EmployeeException;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.enumeration.DidacticFunction;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;


public class GetEmployeeDescTest {
    private String employeeDBFile = "./employees.txt";
    private EmployeeRepositoryImpl employeeRepository = new EmployeeRepositoryImpl();
    private List<Employee> employeeList;
    @Before
    public void setUp() {
        try {
            employeeRepository.cleanRepo();
        } catch (FileNotFoundException e) {
            assertFalse(true);
        }
    }

    @Test
    public void FO3_TC01() {
        Employee employeeOne = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "1000");
        Employee employeeTwo = new Employee("Name","LName","1980707077096" , DidacticFunction.ASISTENT, "100");
        Employee employeeThree = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "100");
        Employee employeeFour = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "10");

        try {
            employeeRepository.addEmployee(employeeTwo);
            employeeRepository.addEmployee(employeeThree);
            employeeRepository.addEmployee(employeeOne);
            employeeRepository.addEmployee(employeeFour);
            employeeList = employeeRepository.getEmployeeDescBySalaryAscByAge();
            assertTrue(employeeList.size() == 4);
            Date a = employeeTwo.getBirthDate();
            Date b = employeeOne.getBirthDate();

            assertTrue(employeeList.get(0).equals(employeeOne));
            assertTrue(employeeList.get(1).equals(employeeTwo));
            assertTrue(employeeList.get(2).equals(employeeThree));
            assertTrue(employeeList.get(3).equals(employeeFour));
        } catch (EmployeeException e) {
            assertFalse(true);
        }
    }

    @Test
    public void FO3_TC02() {
        employeeList = employeeRepository.getEmployeeDescBySalaryAscByAge();
        assertTrue(employeeList.size() == 0);
    }
}
