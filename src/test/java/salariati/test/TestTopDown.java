package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestTopDown {
    private String employeeDBFile = "./employees.txt";
    private EmployeeRepositoryImpl employeeRepository;
    private EmployeeController employeeController;
    private String firstName;
    private int repositorySize;
    private Employee employee;
    private Employee lastAddedEmployee;
    private String cnp;
    private String newCnp;
    private List<Employee> employeeList;

    @Before
    public void setUp() {
        try {
            employeeRepository = new EmployeeRepositoryImpl();
            employeeRepository.cleanRepo();
            employeeController = new EmployeeController(employeeRepository);
        } catch (FileNotFoundException e) {
            assertFalse(true);
        }
    }

    @Test
    public void addEmployee_TC1_EC() {
        // A
        Employee employeeA = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeRepository.addEmployee(employeeA);
        } catch (EmployeeException e) {
            assertFalse(true);
        }

        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employeeA.equals(lastAddedEmployee));

    }

    @Test
    public void FO2_TC01() {
        // B
        Employee employeeB = new Employee("TestFirstName","TestLastName","1970707077091" , DidacticFunction.ASISTENT, "1");
        Employee employeeBModified = new Employee("UpdatedTestFirstName","UpdatedLastName","1970707077091" , DidacticFunction.ASISTENT, "3");
        try {
            employeeRepository.addEmployee(employeeB);
            employeeRepository.modifyEmployee(employeeB.getCnp(), employeeBModified);
            assertTrue(employeeRepository.getEmployeeList().stream().anyMatch((e) -> e.equals(employeeBModified)));
            assertTrue(employeeRepository.getEmployeeList().stream().noneMatch((e) -> e.equals(employeeB)));
        } catch (EmployeeException e) {
            assertFalse(true);
        }
    }
    @Test
    public void FO3_TC01() {
        Employee employeeOne = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "1000");
        Employee employeeTwo = new Employee("Name","LName","1980707077096" , DidacticFunction.ASISTENT, "100");
        Employee employeeThree = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "100");
        Employee employeeFour = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "10");

        try {
            employeeRepository.addEmployee(employeeTwo);
            employeeRepository.addEmployee(employeeThree);
            employeeRepository.addEmployee(employeeOne);
            employeeRepository.addEmployee(employeeFour);
            employeeList = employeeRepository.getEmployeeDescBySalaryAscByAge();
            assertTrue(employeeList.size() == 4);
            Date a = employeeTwo.getBirthDate();
            Date b = employeeOne.getBirthDate();

            assertTrue(employeeList.get(0).equals(employeeOne));
            assertTrue(employeeList.get(1).equals(employeeTwo));
            assertTrue(employeeList.get(2).equals(employeeThree));
            assertTrue(employeeList.get(3).equals(employeeFour));
        } catch (EmployeeException e) {
            assertFalse(true);
        }
    }
    @Test
    public void TopDownPA(){

        // P
        Employee employeeP = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeController.addEmployee("NumePersoana","PrenumePersoana", "1970707077095", "ASISTENT", "1");
        } catch (EmployeeException e) {
            assertFalse(true);
        }

        // A
        Employee employeeA = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeRepository.addEmployee(employeeA);
        } catch (EmployeeException e) {
            assertFalse(true);
        }

        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employeeA.equals(lastAddedEmployee));
    }
    @Test
    public void TopDownPAB(){

        // P
        Employee employeeP = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeController.addEmployee("NumePersoana","PrenumePersoana", "1970707077095", "ASISTENT", "1");
        } catch (EmployeeException e) {
            assertFalse(true);
        }

        // A
        Employee employeeA = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeRepository.addEmployee(employeeA);
        } catch (EmployeeException e) {
            assertFalse(true);
        }

        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employeeA.equals(lastAddedEmployee));

        // B
        Employee employeeB = new Employee("TestFirstName","TestLastName","1970707077091" , DidacticFunction.ASISTENT, "1");
        Employee employeeBModified = new Employee("UpdatedTestFirstName","UpdatedLastName","1970707077091" , DidacticFunction.ASISTENT, "3");
        try {
            employeeRepository.addEmployee(employeeB);
            employeeRepository.modifyEmployee(employeeB.getCnp(), employeeBModified);
            assertTrue(employeeRepository.getEmployeeList().stream().anyMatch((e) -> e.equals(employeeBModified)));
        } catch (EmployeeException e) {
            assertFalse(true);
        }


    }

    @Test
    public void TopDownPABC(){

        // P
        Employee employeeP = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeController.addEmployee("NumePersoana","PrenumePersoana", "1970707077095", "ASISTENT", "1");
        } catch (EmployeeException e) {
            assertFalse(true);
        }

        // A
        Employee employeeA = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeRepository.addEmployee(employeeA);
        } catch (EmployeeException e) {
            assertFalse(true);
        }

        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employeeA.equals(lastAddedEmployee));

        // B
        Employee employeeB = new Employee("TestFirstName","TestLastName","1970707077091" , DidacticFunction.ASISTENT, "1");
        Employee employeeBModified = new Employee("UpdatedTestFirstName","UpdatedLastName","1970707077091" , DidacticFunction.ASISTENT, "3");
        try {
            employeeRepository.addEmployee(employeeB);
            employeeRepository.modifyEmployee(employeeB.getCnp(), employeeBModified);
            assertTrue(employeeRepository.getEmployeeList().stream().anyMatch((e) -> e.equals(employeeBModified)));
        } catch (EmployeeException e) {
            assertFalse(true);
        }

        // C
        Employee employeeOne = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "1000");
        Employee employeeTwo = new Employee("Name","LName","1980707077096" , DidacticFunction.ASISTENT, "100");
        Employee employeeThree = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "100");
        Employee employeeFour = new Employee("Name","LName","1970707077096" , DidacticFunction.ASISTENT, "10");

        try {
            employeeRepository.addEmployee(employeeTwo);
            employeeRepository.addEmployee(employeeThree);
            employeeRepository.addEmployee(employeeOne);
            employeeRepository.addEmployee(employeeFour);
            employeeList = employeeRepository.getEmployeeDescBySalaryAscByAge();
            Date a = employeeTwo.getBirthDate();
            Date b = employeeOne.getBirthDate();
            assertTrue(employeeList.get(0).equals(employeeOne));
            assertTrue(employeeList.get(1).equals(employeeTwo));
            assertTrue(employeeList.get(2).equals(employeeThree));
            assertTrue(employeeList.get(3).equals(employeeFour));
            assertTrue(employeeList.get(4).equals(employeeBModified));
            assertTrue(employeeList.get(5).equals(employeeA));
            assertTrue(employeeList.get(6).equals(employeeP));

        } catch (EmployeeException e) {
            assertFalse(true);
        }
    }

}
