package salariati.test;

import static org.junit.Assert.*;

import salariati.exception.EmployeeException;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.enumeration.DidacticFunction;
import java.io.FileNotFoundException;

public class ModifyEmployeeTest {
	private String employeeDBFile = "./employees.txt";
	private EmployeeRepositoryImpl employeeRepository = new EmployeeRepositoryImpl();
	private String cnp;
	private String newCnp;

	@Before
	public void setUp() {
		try {
			employeeRepository.cleanRepo();
		} catch (FileNotFoundException e) {
			assertFalse(true);
		}
	}
	
	@Test
	public void FO2_TC01() {
		cnp = "1970707077095";
		newCnp = "1970707077096";
		Employee employee = new Employee("TestFirstName","TestLastName",cnp , DidacticFunction.ASISTENT, "1");
		Employee employeeNew = new Employee("UpdatedTestFirstName","UpdatedLastName","1970707077096" , DidacticFunction.ASISTENT, "1");
		try {
			employeeRepository.addEmployee(employee);
			employeeRepository.modifyEmployee(employee.getCnp(), employeeNew);
			assertTrue(employeeRepository.getEmployeeList().size() == 1);
			assertTrue(employeeRepository.getEmployeeList().stream().anyMatch((e) -> e.equals(employeeNew)));
		} catch (EmployeeException e) {
			assertFalse(true);
		}
	}

	@Test
	public void FO2_TC02() {
		cnp = "1970707077095";
		String cnp2 = "1870707077095";
		newCnp = "1970707077096";
		Employee employee = new Employee("TestFirstName","TestLastName",cnp , DidacticFunction.ASISTENT, "1");
		Employee employee2 = new Employee("TestFirstName","TestLastName",cnp2 , DidacticFunction.ASISTENT, "1");
		Employee employeeNew = new Employee("UpdatedTestFirstName","UpdatedLastName",newCnp , DidacticFunction.ASISTENT, "1");
		try {
			employeeRepository.addEmployee(employee2);
			employeeRepository.addEmployee(employee);
			employeeRepository.modifyEmployee(employee.getCnp(), employeeNew);
			assertTrue(employeeRepository.getEmployeeList().size() == 2);
			assertTrue(employeeRepository.getEmployeeList().stream().anyMatch((e) -> e.equals(employeeNew)));
		} catch (EmployeeException e) {
			assertFalse(true);
		}
	}

	@Test
	public void FO2_TC03() {
		cnp = "1970707077095";
		String cnp2 = "1870707077095";
		newCnp = "1970707077096";
		Employee employee = new Employee("TestFirstName","TestLastName",cnp , DidacticFunction.ASISTENT, "1");
		Employee employeeNew = new Employee("UpdatedTestFirstName","UpdatedLastName",newCnp , DidacticFunction.ASISTENT, "1");
		try {
			employeeRepository.addEmployee(employee);
			employeeRepository.modifyEmployee(employee.getCnp()+"dsada", employeeNew);
			assertFalse(true);
		} catch (EmployeeException e) {
			assertTrue(true);
		}
	}

	@Test
	public void FO2_TC04() {
		cnp = "1970707077095";
		String cnp2 = "1870707077095";
		newCnp = "1970707077095";
		Employee employeeNew = new Employee("UpdatedTestFirstName","UpdatedLastName",newCnp , DidacticFunction.ASISTENT, "1");
		try {
			employeeRepository.modifyEmployee(cnp, employeeNew);
			assertFalse(true);
		} catch (EmployeeException e) {
			assertTrue(true);
		}
	}

	@Test
	public void FO2_TC05() {
//		cnp = "1970707077095";
//		String cnp2 = "1870707077095";
//		newCnp = "1970707077095";
//		Employee employee = new Employee("TestFirstName","TestLastName",cnp , DidacticFunction.ASISTENT, "1");
//		Employee employeeNew = new Employee("UpdatedTestFirstName","UpdatedLastName",newCnp , DidacticFunction.ASISTENT, "1");
//		try {
//			employeeRepository.addEmployee(employee);
//			File file = new File(employeeDBFile);
//			if(file.delete()){
//				assertFalse(true);
//			}
//			employeeRepository.modifyEmployee(employee.getCnp(), employeeNew);
//
//			employeeRepository.modifyEmployee(cnp, employeeNew);
//			assertFalse(true);
//		} catch (EmployeeException e) {
//			assertTrue(true);
//		}
	}

}
