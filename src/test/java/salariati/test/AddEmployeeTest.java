package salariati.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeRepositoryMock;

import java.io.FileNotFoundException;
import java.math.BigInteger;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AddEmployeeTest {
    private String employeeDBFile = "./employees.txt";
    private EmployeeRepositoryImpl employeeRepository = new EmployeeRepositoryImpl();
    private String firstName;
    private int repositorySize;
    private Employee employee;
    private Employee lastAddedEmployee;

    @Before
    public void setUp() {
        try {
            employeeRepository.cleanRepo();
        } catch (FileNotFoundException e) {
            assertFalse(true);
        }
    }

    @Test
    public void addEmployee_TC1_EC() {
        StringBuilder b = new StringBuilder(256);
        for (int i = 0; i < 256; i += 1) {
            b.append("a");
        }
        firstName = b.toString();
        repositorySize = employeeRepository.getEmployeeList().size();
        employee = new Employee(firstName,"PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try{
            employeeRepository.addEmployee(employee);
            assertFalse(true);
        }catch (EmployeeException ex){
            assertTrue(ex.getMessage().equals("First name is invalid, too many characters. "));
            assertTrue(repositorySize == employeeRepository.getEmployeeList().size());
        }
    }

    @Test
    public void addEmployee_TC3_EC() {
        //TC1_EC
        employee = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeRepository.addEmployee(employee);
        } catch (EmployeeException e) {
            assertFalse(true);
        }
        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employee.equals(lastAddedEmployee));
    }

    @Test
    public void addEmployee_TC5_EC() {
        //TC5_EC
        repositorySize = employeeRepository.getEmployeeList().size();
        employee = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "-1");
        try{
            employeeRepository.addEmployee(employee);
            assertFalse(true);
        }catch (EmployeeException ex){
            assertTrue(ex.getMessage().equals("Salary is invalid, must be greater than 0. "));
            assertTrue(repositorySize == employeeRepository.getEmployeeList().size());
        }
    }

    @Test
    public void addEmployee_TC1_BVA() {
        //TC1_BVA
        repositorySize = employeeRepository.getEmployeeList().size();
        employee = new Employee("","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try{
            employeeRepository.addEmployee(employee);
            assertFalse(true);
        }catch (EmployeeException ex){
            assertTrue(ex.getMessage().equals("First name is invalid, cannot be empty "));
            assertTrue(repositorySize == employeeRepository.getEmployeeList().size());
        }
    }

    @Test
    public void addEmployee_TC2_BVA() {
        //TC2_BVA
        employee = new Employee("a","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeRepository.addEmployee(employee);
        } catch (EmployeeException e) {
            assertFalse(true);
        }
        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employee.equals(lastAddedEmployee));
    }

    @Test
    public void addEmployee_TC3_BVA() {
        //TC3_BVA
        employee = new Employee("aa","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try {
            employeeRepository.addEmployee(employee);
        } catch (EmployeeException e) {
            assertFalse(true);
        }
        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employee.equals(lastAddedEmployee));
    }

    @Test
    public void addEmployee_TC4_BVA() {
        //TC4_BVA
        StringBuilder b = new StringBuilder(254);
        for (int i = 0; i < 254; i += 1) {
            b.append("a");
        }
        firstName = b.toString();
        repositorySize = employeeRepository.getEmployeeList().size();
        employee = new Employee(firstName,"PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try{
            employeeRepository.addEmployee(employee);
        }catch (EmployeeException ex){
            assertFalse(true);
        }
        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employee.equals(lastAddedEmployee));
    }

    @Test
    public void addEmployee_TC5_BVA() {
        //TC5_BVA
        StringBuilder b = new StringBuilder(255);
        for (int i = 0; i < 255; i += 1) {
            b.append("a");
        }
        firstName = b.toString();
        repositorySize = employeeRepository.getEmployeeList().size();
        employee = new Employee(firstName,"PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "1");
        try{
            employeeRepository.addEmployee(employee);
        }catch (EmployeeException ex){
            assertFalse(true);
        }
        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employee.equals(lastAddedEmployee));
    }

    @Test
    public void addEmployee_TC8_BVA() {
        //TC8_BVA
        repositorySize = employeeRepository.getEmployeeList().size();
        employee = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, "0");
        try{
            employeeRepository.addEmployee(employee);
            assertFalse(true);
        }catch (EmployeeException ex){
            assertTrue(ex.getMessage().equals("Salary is invalid, must be greater than 0. "));
            assertTrue(repositorySize == employeeRepository.getEmployeeList().size());
        }
    }

    @Test
    public void addEmployee_TC10_BVA() {
        //TC10_BVA
        employee = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, String.valueOf(Integer.MAX_VALUE-1));
        try {
            employeeRepository.addEmployee(employee);
        } catch (EmployeeException e) {
            assertFalse(true);
        }
        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employee.equals(lastAddedEmployee));
    }

    @Test
    public void addEmployee_TC11_BVA() {
        //TC11_BVA
        employee = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, String.valueOf(Integer.MAX_VALUE));
        try {
            employeeRepository.addEmployee(employee);
        } catch (EmployeeException e) {
            assertFalse(true);
        }
        lastAddedEmployee = employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1);
        assertTrue(employee.equals(lastAddedEmployee));
    }

    @Test
    public void addEmployee_TC12_BVA() {
        repositorySize = employeeRepository.getEmployeeList().size();
        employee = new Employee("NumePersoana","PrenumePersoana", "1970707077095", DidacticFunction.ASISTENT, String.valueOf(BigInteger.valueOf(Integer.MAX_VALUE).add(BigInteger.ONE)));
        try{
            employeeRepository.addEmployee(employee);
            assertFalse(true);
        }catch (EmployeeException ex){
            assertTrue(ex.getMessage().equals("Salary is invalid, number is too big"));
            assertTrue(repositorySize == employeeRepository.getEmployeeList().size());
        }
    }

    @After
    public void tearDown() throws Exception {

    }
}
