package salariati.ui;

import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.util.Scanner;

public class ConsoleUi {

    private EmployeeController employeeController;

    public ConsoleUi(EmployeeRepositoryInterface employeeRepository){
        this.employeeController = new EmployeeController(employeeRepository);
    }

    public void run(){
        boolean appRunning = true;
        Scanner sc = new Scanner(System.in);
        while(appRunning){
            System.out.println("------------------");
            System.out.println("1. Display all employees sorted descending by salary and ascending by age");
            System.out.println("2. Add a new employee");
            System.out.println("3. Modify an existing employee. ");
            System.out.println("4. Exit. ");
            System.out.println("------------------");
            System.out.println("Option: ");

            int option = sc.nextInt();

            switch (option){
                case 1:
                    for(Employee emp : employeeController.getEmployeeDescBySalaryAscByAge()){
                        System.out.println(emp.toString());
                    }
                    break;
                case 2:
                    System.out.println("First Name:");
                    String firstName = sc.next();

                    System.out.println("Last Name:");
                    String lastName = sc.next();

                    System.out.println("Salary:");
                    String salary = sc.next();

                    System.out.println("CNP:");
                    String cnp = sc.next();

                    System.out.println("DIDACTIC FUNCTION:");
                    String didacticFunctionString = sc.next();
                    try{

                    }catch (Exception ex){
                        System.out.println("Invalid function entered. ");
                        break;
                    }
                    try {
                        employeeController.addEmployee(firstName,lastName, cnp, didacticFunctionString, salary);
                    } catch (EmployeeException e) {
                        System.out.println(e.getMessage());
                    }
                    System.out.println("Successfuly created employee. ");
                    break;
                case 3:
                    System.out.println("Please enter the CNP of the employee you would like to modify:");
                    String cnpEmployee = sc.next();

                    System.out.println("First Name:");
                    String newFirstName = sc.next();

                    System.out.println("Last Name:");
                    String newLastName = sc.next();

                    System.out.println("Salary:");
                    String newSalary = sc.next();

                    System.out.println("DIDACTIC FUNCTION:");
                    String newDidacticFunctionString = sc.next();

                    DidacticFunction newDidacticFunction = DidacticFunction.ASISTENT;
                    try{
                        newDidacticFunction = DidacticFunction.valueOf(newDidacticFunctionString.toUpperCase());
                    }catch (Exception ex){
                        System.out.println("Invalid function entered. ");
                        break;
                    }
                    try {
                        employeeController.modifyEmployee(cnpEmployee,new Employee(newFirstName, newLastName, cnpEmployee, newDidacticFunction, newSalary));
                    } catch (EmployeeException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Successfuly modified employee. ");
                    break;
                case 4:
                    appRunning = false;
                    break;
                default:
                    System.out.println("Please enter a valid option. ");
                    break;
            }
        }
    }

}
