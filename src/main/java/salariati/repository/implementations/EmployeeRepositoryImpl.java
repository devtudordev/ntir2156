package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeRepositoryImpl implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "./employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public void addEmployee(Employee employee) throws EmployeeException{
		employeeValidator.validate(employee);
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
			bw.write(employee.toString());
			bw.newLine();
			bw.close();

		} catch (IOException e) {
			throw new EmployeeException(e.getMessage());
		}

	}

	@Override
	public void deleteEmployee(Employee employee){
		// TODO Auto-generated method stub
		
	}

	public void cleanRepo() throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter(employeeDBFile);
		printWriter.print("");
		printWriter.close();
	}

	@Override
	public void modifyEmployee(String employeeCNP, Employee newEmployee) throws EmployeeException {
		employeeValidator.validate(newEmployee);
		List<Employee> emps = getEmployeeList();
		Employee employee = null;
		for(int i = 0 ; i< emps.size() ; i++)
		{
			Employee emp = emps.get(i);
			if(emp.getCnp().equals(employeeCNP)){
				employee = emp;
			}
		}
		if(employee == null){
			throw new EmployeeException("Employee not present");
		}
		employee.setCnp(newEmployee.getCnp());
		employee.setFirstName(newEmployee.getFirstName());
		employee.setLastName(newEmployee.getLastName());
		employee.setFunction(newEmployee.getFunction());
		employee.setSalary(newEmployee.getSalary());

		// clear the employees in the file
		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(employeeDBFile);
		} catch (Exception e) {
			throw new EmployeeException("Error parsing file. ");
		}
		printWriter.print("");
		printWriter.close();

		for(int i = 0 ; i< emps.size() ; i++)
		{
			addEmployee(emps.get(i));
		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = (Employee) Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
					counter++;
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeDescBySalaryAscByAge() {
		Comparator<Employee> comparator = new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				return Integer.parseInt(o2.getSalary()) - Integer.parseInt(o1.getSalary());
			}
		};

		comparator = comparator.thenComparing((o1, o2) -> o2.getBirthDate().compareTo(o1.getBirthDate()));

		return getEmployeeList()
				.stream()
				.sorted(comparator)
				.collect(Collectors.toList());
	}

}
