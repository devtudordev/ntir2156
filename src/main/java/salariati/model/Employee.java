package salariati.model;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.validator.EmployeeValidator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Employee {

	/** The first name of the employee */
	private String firstName;

	/** The last name of the employee */
	private String lastName;

	/** The unique id of the employee */
	private String cnp;
	
	/** The didactic function of the employee inside the university */
	private DidacticFunction function;
	
	/** The salary of the employee */
	private String salary;

	/**
	 * Default constructor for employee
	 */
	public Employee() {
		this.firstName = "";
		this.lastName  = "";
		this.cnp       = "";
		this.function  = DidacticFunction.ASISTENT;
		this.salary    = "";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(firstName, employee.firstName) &&
				Objects.equals(lastName, employee.lastName) &&
				Objects.equals(cnp, employee.cnp) &&
				function == employee.function &&
				Objects.equals(salary, employee.salary);
	}

	@Override
	public int hashCode() {
		return Objects.hash(firstName, lastName, cnp, function, salary);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public DidacticFunction getFunction() {
		return function;
	}

	public void setFunction(DidacticFunction function) {
		this.function = function;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public Employee(String firstName, String lastName, String cnp, DidacticFunction function, String salary) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.cnp = cnp;
		this.function = function;
		this.salary = salary;
	}

	/*
     * Returns the birth date of an employee based on the CNP
	 */
	public Date getBirthDate(){
		Date date = new Date();
		if(this.cnp.length()<13){
			return date;
		}
		String dateString = "";

		// Determine first 2 digits of the year
		switch(cnp.charAt(0)){
			case '1':
				dateString += "19";
				break;
			case '2':
				dateString += "19";
				break;
			case '3':
				dateString += "18";
				break;
			case '4':
				dateString += "18";
				break;
			case '5':
				dateString += "20";
				break;
			case '6':
				dateString += "20";
				break;
			case '7':
				dateString += "19";
				break;
			case '8':
				dateString += "19";
				break;
			default:
				return new Date();
		}
		String lastTwoDigitsYear = cnp.substring(1,3);
		String month = cnp.substring(3,5);
		String day = cnp.substring(5,7);
		dateString+= lastTwoDigitsYear + "/" + month + "/" + day;
		DateFormat format =  new SimpleDateFormat("yyyy/MM/dd");
		try {
			date = format.parse(dateString);
		} catch (ParseException e) {
			return date;
		}
		return date;
	}

	/**
	 * toString function for employee
	 */
	@Override
	public String toString() {
		String employee = "";

		employee += firstName + ";";
		employee += lastName + ";";
		employee += cnp + ";";
		employee += function.toString() + ";";
		employee += salary;

		return employee;
	}

	/**
	 * Returns the Employee after parsing the given line
	 *
	 * @param _employee
	 *            the employee given as String from the input file
	 * @param line
	 *            the current line in the file
	 * 
	 * @return if the given line is valid returns the corresponding Employee
	 * @throws EmployeeException
	 */
	public static Employee getEmployeeFromString(String _employee, int line) throws EmployeeException {
		Employee employee = new Employee();
		
		String[] attributes = _employee.split("[;]");
		
		if( attributes.length != 5 ) {
			throw new EmployeeException("Invalid line at: " + line);
		} else {
			EmployeeValidator validator = new EmployeeValidator();
			employee.setFirstName(attributes[0]);
			employee.setLastName(attributes[1]);
			employee.setCnp(attributes[2]);
			
			if(attributes[3].equals("ASISTENT"))
				employee.setFunction(DidacticFunction.ASISTENT);
			if(attributes[3].equals("LECTURER"))
				employee.setFunction(DidacticFunction.LECTURER);
			if(attributes[3].equals("TEACHER"))
				employee.setFunction(DidacticFunction.TEACHER);
			if(attributes[3].equals("CONFERENTIAR"))
				employee.setFunction(DidacticFunction.CONFERENTIAR);

			employee.setSalary(attributes[4]);
			validator.validate(employee);
		}

		return employee;
	}

}
