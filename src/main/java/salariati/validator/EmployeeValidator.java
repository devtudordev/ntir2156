package salariati.validator;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;

import java.math.BigInteger;

public class EmployeeValidator {
	
	public EmployeeValidator(){}

	public void validate(Employee employee) throws EmployeeException {
		boolean isLastNameValid  = employee.getLastName().matches("[a-zA-Z]+") && (employee.getLastName().length() > 2);
		boolean isCNPValid       = employee.getCnp().matches("[a-z0-9]+") && (employee.getCnp().length() == 13);
		boolean isFunctionValid  = employee.getFunction().equals(DidacticFunction.ASISTENT) ||
								   employee.getFunction().equals(DidacticFunction.LECTURER) ||
								   employee.getFunction().equals(DidacticFunction.TEACHER);

		// First name validation
		if(employee.getFirstName().length() > 255){
			throw new EmployeeException("First name is invalid, too many characters. ");
		}
		if(employee.getFirstName().length() == 0){
			throw new EmployeeException("First name is invalid, cannot be empty ");
		}

		// Salary validation
		try{
			BigInteger maxInt = BigInteger.valueOf(Integer.MAX_VALUE);
			BigInteger value = new BigInteger(employee.getSalary());

			if (value.compareTo(maxInt) > 0)
			{
				throw new EmployeeException("Salary is invalid, number is too big");
			}
			if(value.compareTo( BigInteger.ZERO) <= 0){
				throw new EmployeeException("Salary is invalid, must be greater than 0. ");
			}
		}catch (NumberFormatException ex){
			throw new EmployeeException("Salary must be a number.");
		}
		if(!isLastNameValid)
			throw new EmployeeException("Last name not valid. ");
		if(!isCNPValid)
			throw new EmployeeException("CNP not valid. ");
		if(!isFunctionValid)
			throw new EmployeeException("Function not valid. ");
	}

	
}
