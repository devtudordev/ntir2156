package salariati.controller;

import java.util.List;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;

	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	public List<Employee> getEmployeeDescBySalaryAscByAge(){
		return employeeRepository.getEmployeeDescBySalaryAscByAge();
	}
	public void addEmployee(String firstName, String lastName, String cnp, String didacticFunctionString, String salary) throws EmployeeException {
		DidacticFunction didacticFunction = DidacticFunction.valueOf(didacticFunctionString);
		employeeRepository.addEmployee(new Employee(firstName,lastName, cnp, didacticFunction, salary));
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(String employeeCNP, Employee newEmployee) throws EmployeeException {
        employeeRepository.modifyEmployee(employeeCNP, newEmployee);
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}
	
}
